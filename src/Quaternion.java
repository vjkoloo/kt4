import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** Quaternions. Basic operations. */
public class Quaternion {

   private double a, b, c, d;

   private static double min = 0.000001;

   /** Constructor from four double values.
    * @param a real part
    * @param b imaginary part i
    * @param c imaginary part j
    * @param d imaginary part k
    */
   public Quaternion (double a, double b, double c, double d) {
      this.a = a;
      this.b = b;
      this.c = c;
      this.d = d;
   }

   /** Real part of the quaternion.
    * @return real part
    */
   public double getRpart() {
      return a;
   }

   /** Imaginary part i of the quaternion.
    * @return imaginary part i
    */
   public double getIpart() {
      return b;
   }

   /** Imaginary part j of the quaternion.
    * @return imaginary part j
    */
   public double getJpart() {
      return c;
   }

   /** Imaginary part k of the quaternion.
    * @return imaginary part k
    */
   public double getKpart() {
      return d;
   }

   /** Conversion of the quaternion to the string.
    * @return a string form of this quaternion:
    * "a+bi+cj+dk"
    * (without any brackets)
    */
   @Override
   public String toString() {
      StringBuilder s = new StringBuilder();
      double[] numbers = new double[] {a, b, c, d};
      String[] letters = new String[] {"", "i", "j", "k"};
      int alg = 0;
      for (int i = 0; i < numbers.length; i++) {
         String sign = (i > alg && numbers[i] >= 0) ? "+" : "";
         if (numbers[i] >= -min && numbers[i] <= min) {
            alg++;
            continue;
         }
         s.append(sign);
         if (numbers[i] % 1 == 0)
            s.append((int)numbers[i]);
         else
            s.append(numbers[i]);
         s.append(letters[i]);
      }
      if (s.length() == 0)
         return "0";
      return s.toString();
   }

   /** Conversion from the string to the quaternion.
    * Reverse to <code>toString</code> method.
    * @throws IllegalArgumentException if string s does not represent
    * a quaternion (defined by the <code>toString</code> method)
    * @param s string of form produced by the <code>toString</code> method
    * @return a quaternion represented by string s
    */
   public static Quaternion valueOf (String s) {
      if (!s.matches("([-]?\\d+\\.?(\\d+)?)?([-+]?\\d+\\.?(\\d+)?i)?([-+]?\\d+\\.?(\\d+)?j)?([-+]?\\d+\\.?(\\d+)?k)?")
              || s.isEmpty()) {
         throw new RuntimeException("String " + s + " does not represent a quaternion!");
      }
      String nums = s.replaceAll("\\+", " ").replaceAll("-", " -").trim();
      String[] parts = nums.split(" ");
      Quaternion value = new Quaternion(0, 0, 0, 0);
      for (String part : parts) {
         double num = Double.parseDouble(part.replaceAll("[i-k]", ""));
         switch (part.substring(part.length() - 1)) {
            default -> value.a = num;
            case "i" -> value.b = num;
            case "j" -> value.c = num;
            case "k" -> value.d = num;
         }
      }
      return value;
   }

   /** Clone of the quaternion.
    * @return independent clone of <code>this</code>
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Quaternion(getRpart(), getIpart(), getJpart(), getKpart());
   }

   /** Test whether the quaternion is zero.
    * @return true, if the real part and all the imaginary parts are (close to) zero
    */
   public boolean isZero() {
      if (Math.abs(getRpart()) > min || Math.abs(getIpart()) > min || Math.abs(getJpart()) > min || Math.abs(getKpart()) > min) {
         return false;
      }
      return true;
   }

   /** Conjugate of the quaternion. Expressed by the formula
    *     conjugate(a+bi+cj+dk) = a-bi-cj-dk
    * @return conjugate of <code>this</code>
    */
   public Quaternion conjugate() {
      return new Quaternion(getRpart(), 0 - getIpart(), 0 - getJpart(),0 - getKpart());
   }

   /** Opposite of the quaternion. Expressed by the formula
    *    opposite(a+bi+cj+dk) = -a-bi-cj-dk
    * @return quaternion <code>-this</code>
    */
   public Quaternion opposite() {
      return new Quaternion(0 - getRpart(), 0 - getIpart(), 0 - getJpart(),0 - getKpart());
   }

   /** Sum of quaternions. Expressed by the formula
    *    (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
    * @param q addend
    * @return quaternion <code>this+q</code>
    */
   public Quaternion plus (Quaternion q) {
      return new Quaternion(
              this.getRpart() + q.getRpart(),
              this.getIpart() + q.getIpart(),
              this.getJpart() + q.getJpart(),
              this.getKpart() + q.getKpart());
   }

   /** Product of quaternions. Expressed by the formula
    *  (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
    *  (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
    * @param q factor
    * @return quaternion <code>this*q</code>
    */
   public Quaternion times (Quaternion q) {
      return new Quaternion(
              this.getRpart() * q.getRpart() - this.getIpart() * q.getIpart() - this.getJpart() * q.getJpart() - this.getKpart() * q.getKpart(),
              this.getRpart() * q.getIpart() + this.getIpart() * q.getRpart() + this.getJpart() * q.getKpart() - this.getKpart() * q.getJpart(),
              this.getRpart() * q.getJpart() - this.getIpart() * q.getKpart() + this.getJpart() * q.getRpart() + this.getKpart() * q.getIpart(),
              this.getRpart() * q.getKpart() + this.getIpart() * q.getJpart() - this.getJpart() * q.getIpart() + this.getKpart() * q.getRpart());
   }

   /** Multiplication by a coefficient.
    * @param r coefficient
    * @return quaternion <code>this*r</code>
    */
   public Quaternion times (double r) {
      return new Quaternion(r * getRpart()
              , r * getIpart()
              , r * getJpart(),
              r * getKpart());
   }

   /** Inverse of the quaternion. Expressed by the formula
    *     1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) +
    *     ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
    * @return quaternion <code>1/this</code>
    */
   public Quaternion inverse() {
      if (this.isZero()) {
         throw new RuntimeException(String.format("Cannot inverse %s as it is zero.", this));
      }
      double divider = getRpart() * getRpart() + getIpart() * getIpart() + getJpart() * getJpart() + getKpart() * getKpart();
      return new Quaternion(
              getRpart() / divider,
              (0 - getIpart()) / divider,
              (0 - getJpart()) / divider,
              (0 - getKpart()) / divider);
   }

   /** Difference of quaternions. Expressed as addition to the opposite.
    * @param q subtrahend
    * @return quaternion <code>this-q</code>
    */
   public Quaternion minus (Quaternion q) {
      return new Quaternion(
              this.getRpart() - q.getRpart(),
              this.getIpart() - q.getIpart(),
              this.getJpart() - q.getJpart(),
              this.getKpart() - q.getKpart());
   }

   /** Right quotient of quaternions. Expressed as multiplication to the inverse.
    * @param q (right) divisor
    * @return quaternion <code>this*inverse(q)</code>
    */
   public Quaternion divideByRight (Quaternion q) {
      if (q.isZero()) {
         throw new RuntimeException(String.format("Cannot divide %s by %s as %s is zero.", this, q, q));
      }
      return times(q.inverse());
   }

   /** Left quotient of quaternions.
    * @param q (left) divisor
    * @return quaternion <code>inverse(q)*this</code>
    */
   public Quaternion divideByLeft (Quaternion q) {
      if (q.isZero()) {
         throw new RuntimeException(String.format("Cannot divide %s by %s as %s is zero.", q, this, this));
      }
      return (q.inverse().times(this));
   }

   /** Equality test of quaternions. Difference of equal numbers
    *     is (close to) zero.
    * @param qo second quaternion
    * @return logical value of the expression <code>this.equals(qo)</code>
    */
   @Override
   public boolean equals (Object qo) {
      if (Math.abs(this.a - ((Quaternion) qo).a) > min) {
         return false;
      }
      if (Math.abs(this.b - ((Quaternion) qo).b) > min) {
         return false;
      }
      if (Math.abs(this.c - ((Quaternion) qo).c) > min) {
         return false;
      }
      if (Math.abs(this.d - ((Quaternion) qo).d) > min) {
         return false;
      }
      return true;
   }

   /** Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
    * @param q factor
    * @return dot product of this and q
    */
   public Quaternion dotMult (Quaternion q) {
      return this.times(q.conjugate()).plus(q.times(this.conjugate())).times(0.5);
   }

   /** Integer hashCode has to be the same for equal objects.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      int hash = 31;
      hash = 31 * hash + (int) getRpart();
      hash = 31 * hash + (int) getIpart();
      hash = 31 * hash + (int) getJpart();
      hash = 31 * hash + (int) getKpart();

      return hash;
   }

   /** Norm of the quaternion. Expressed by the formula
    *     norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
    * @return norm of <code>this</code> (norm is a real number)
    */
   public double norm() {
      return Math.sqrt(this.getRpart() * this.getRpart()
              + this.getIpart() * this.getIpart()
              + this.getJpart() * this.getJpart()
              + this.getKpart() * this.getKpart());
   }

   /** Main method for testing purposes.
    * @param arg command line parameters
    */
   public static void main (String[] arg) {
      Quaternion arv1 = new Quaternion (-1., 1, 2., -2.);
      if (arg.length > 0)
         arv1 = valueOf (arg[0]);
      System.out.println ("first: " + arv1.toString());
      System.out.println ("real: " + arv1.getRpart());
      System.out.println ("imagi: " + arv1.getIpart());
      System.out.println ("imagj: " + arv1.getJpart());
      System.out.println ("imagk: " + arv1.getKpart());
      System.out.println ("isZero: " + arv1.isZero());
      System.out.println ("conjugate: " + arv1.conjugate());
      System.out.println ("opposite: " + arv1.opposite());
      System.out.println ("hashCode: " + arv1.hashCode());
      Quaternion res = null;
      try {
         res = (Quaternion)arv1.clone();
      } catch (CloneNotSupportedException e) {};
      System.out.println ("clone equals to original: " + res.equals (arv1));
      System.out.println ("clone is not the same object: " + (res!=arv1));
      System.out.println ("hashCode: " + res.hashCode());
      res = valueOf (arv1.toString());
      System.out.println ("string conversion equals to original: "
              + res.equals (arv1));
      Quaternion arv2 = new Quaternion (1., -2.,  -1., 2.);
      if (arg.length > 1)
         arv2 = valueOf (arg[1]);
      System.out.println ("second: " + arv2.toString());
      System.out.println ("hashCode: " + arv2.hashCode());
      System.out.println ("equals: " + arv1.equals (arv2));
      res = arv1.plus (arv2);
      System.out.println ("plus: " + res);
      System.out.println ("times: " + arv1.times (arv2));
      System.out.println ("minus: " + arv1.minus (arv2));
      double mm = arv1.norm();
      System.out.println ("norm: " + mm);
      System.out.println ("inverse: " + arv1.inverse());
      System.out.println ("divideByRight: " + arv1.divideByRight (arv2));
      System.out.println ("divideByLeft: " + arv1.divideByLeft (arv2));
      System.out.println ("dotMult: " + arv1.dotMult (arv2));
   }
}
// end of file